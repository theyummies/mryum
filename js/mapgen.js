let fs = require('fs');

const cosik = require("../vendor/assets/cosik.json")


function boundariesMap(width, height){

    function mahRand(min = 0,max = 1)
    {
        let rand = Math.floor(Math.random()*(max-min+1)+min)
        return (rand == 0 ? 0 : 9 );
    }

    let ints = "1";
    ints += "5".repeat(width - 2)
    ints += "4";

    for(let i = 1; i < height - 1; i++){
        for(let j = 0; j < width; j++){
            if((j == 0)){
                ints += 8;
            }
            else if (j == width - 1){
                ints += 7;
            }
            else{
                ints += 9//mahRand();
            }
        }
    }

    ints += "2";
    ints += "6".repeat(width - 2);
    ints += "3"

    ints = ints.replace(/(.)/g,"$1,")
    ints = ints.substring(0, ints.length - 1)

    let map = "[" + ints + "]"
    return JSON.parse(map)
}

//
// function dirtGen(width, height){
//
//     function mahRand(min = 0,max = 1)
//     {
//         let rand = Math.floor(Math.random()*(max-min+1)+min)
//         return (rand == 0 ? 0 : 9 );
//     }
//
//     let ints = "0".repeat(width)
//
//     for(let i = 1; i < height - 1; i++){
//         for(let j = 0; j < width; j++){
//             if((j == 0 || j == width - 1)){
//                 ints += 0;
//             }
//             else{
//                 ints += mahRand();
//             }
//         }
//     }
//
//     ints += "0".repeat(width);
//
//     ints = ints.replace(/(.)/g,"$1,")
//     ints = ints.substring(0, ints.length - 1)
//
//     let map = "[" + ints + "]"
//     return JSON.parse(map)
// }

// console.log(dirtGen(10,10))

let width = 60;
let height = 100;

let map = (cosik.layers.find( (val) => { return val.name == "collision" } ))
map.data = boundariesMap(width, height);
map.height = height;
map.width = width;

//
// let dirt = (cosik.layers.find( (val) => { return val.name == "dirt" } ))
// dirt.data = dirtGen(width, height);
// dirt.height = height;
// dirt.width = width;



fs.writeFile("../vendor/assets/cosik.json", JSON.stringify(cosik, null, 4), function(err, data){
    if (err) console.log("[ERROR] error while updating ansible inventory file " + err);
    //console.log("[INFO] Ansible inventory successfully updated");
})