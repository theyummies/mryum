const { Layer, Network } = window.synaptic;

var inputLayer = new Layer(8*8);
var hiddenLayer = new Layer(8*8);
var outputLayer = new Layer(8*8);

inputLayer.project(hiddenLayer);
hiddenLayer.project(outputLayer);

var myNetwork = new Network({
    input: inputLayer,
    hidden: [hiddenLayer],
    output: outputLayer
});

const canvasWidth = window.innerWidth;
const canvasHeight = window.innerHeight;
var learningRate = .3;

let game = new Phaser.Game(canvasWidth, canvasHeight, Phaser.CANVAS, "Mr Yum", { preload: preload, create: create, update: update, render: render });
game.config.forceSetTimeOut = true;

const MOVE = {
    IDLE: "idle", UP: "up", DOWN: "down", RIGHT: "right", LEFT: "left"
}

const stageLimit = 300000; // 5 minutes

let moving = false;

let fps = 10;
let char;
let moveTimer;
let collisionLayer;
let dirtLayer;
let tunnelLayer;
let playerInitPosition = [45 * 5, 45 * 5];
let playerPosition = JSON.parse(JSON.stringify(playerInitPosition));
let moveset = [{direction: MOVE.IDLE, time: 0, position: JSON.parse(JSON.stringify(playerPosition))}];
let normalSpeed = 450;
let mahSpeed = 450;
let cursors;
let clones = [];
let clonesGroup;
let cloneTimer;
let cloneInterval = 8000 // 8 seconds
let health = 10; // 6 seconds
let healthTimer;
let healthBar;
let stateText;
let level = 1;
let exp = 0;
let pickupFood, pickupBouns;

function preload(){
    game.stage.disableVisibilityChange = true;

    // FontBuilder format: Sparrow
    game.load.bitmapFont("branding", "vendor/assets/font/branding_bold_45.PNG", "vendor/assets/font/branding_bold_45.fnt");

    game.load.tilemap("map", "vendor/assets/cosik.json?stamp=" + Date.now(), null, Phaser.Tilemap.TILED_JSON);
    game.load.image('ground_1x1', 'vendor/assets/border.png');

    game.load.spritesheet("char", "vendor/assets/player.png?stemp=" + Date.now(), 45, 45);
    game.load.spritesheet("clone", "vendor/assets/badguy.png?stemp=" + Date.now(), 45, 45);
    game.load.spritesheet("health", "vendor/assets/health.png", 120, 110);
    game.load.spritesheet("pickup-food", "vendor/assets/bonus.png", 45, 45);
    game.load.spritesheet("bonus", "vendor/assets/bonus_storka.png", 45, 45);

    game.load.audio('eat', 'vendor/assets/sounds/eat.mp3');

}

function create(){
    game.stage.backgroundColor = "#333";
    game.physics.startSystem(Phaser.Physics.ARCADE);

    map = game.add.tilemap("map");

    map.addTilesetImage('ground_1x1');

    collisionLayer = map.createLayer("collision");
    collisionLayer.resizeWorld();
    collisionLayer.debugSettings.forceFullRedraw = true;

    clonesGroup = game.add.group();
    pickupFood = game.add.group();
    pickupBouns = game.add.group();

    stateText = game.add.bitmapText(canvasWidth / 2, canvasHeight / 2, "branding");
    stateText.anchor.setTo(0.5, 0.5);
    stateText.fixedToCamera = true;
    stateText.visible = false;

    char = game.add.sprite(playerPosition[0], playerPosition[1], "char", 0);
    char.anchor.setTo(0.5);
    char.inputEnabled = true;

    char.animations.add("idle", [0,1,2,3], fps, true);
    char.animations.add("bottom", [0,1,2,3], fps, true);
    char.animations.add("left", [0,1,2,3], fps, true);
    char.animations.add("right", [0,1,2,3], fps, true);
    char.animations.add("top", [0,1,2,3], fps, true);
    char.animations.add("dig", [0,1,2,3], fps, true);
    char.animations.add("death", [15,16,17,18,19,20,21,22,23,24,25,26,27,28], 15, false);

    eat = game.add.audio('eat');

    game.physics.enable(char);
    game.camera.follow(char);
    cursors = game.input.keyboard.createCursorKeys();

    healthBar = game.add.sprite(canvasWidth - 200, 25, "health", 0);
    healthBar.fixedToCamera = true;
    healthBar.animations.add("run", null, (23 / health), false);
    healthBar.animations.play("run");

    cloneTimer = game.time.create(false);
    cloneTimer.loop(cloneInterval, spawnClone, this);
    cloneTimer.start();

    moveTimer = game.time.create(true);
    moveTimer.loop(stageLimit, function(){}, this);
    moveTimer.start();

    healthTimer = game.time.create(false);
    healthTimer.loop(health * 1000, healthTick, this);
    healthTimer.start();

    map.replace(9, -1, parseInt(playerPosition[0] / 45) - 1, parseInt(playerPosition[1] / 45) - 1, 2, 2);
    map.setCollisionBetween(1, 200, true, "collision", true);

    spawnFood();
    spawnBonus()
    spawnFood();
    spawnBonus()
}

function render(){
    game.debug.text("Player position: [" + parseInt(playerPosition[0]) + "," + parseInt(playerPosition[1]) + "]", 32, 32);
    game.debug.text("Next clone will spawn in: " + parseInt(Math.ceil(cloneTimer.duration.toFixed(0) / 1000)) + "s", 32, 64);
    game.debug.text("Active clones: " + clones.length, 32, 96);
    game.debug.text("Hunger bar: " + parseInt(Math.ceil(healthTimer.duration.toFixed(0) / 1000)), 400, 32);
    game.debug.text("Game over: " + !char.alive, 400, 64);
    game.debug.text("Level: " + level, 650, 32);
    game.debug.text("Exp points: " + exp, 650, 64);
}

function update(){
    game.physics.arcade.collide(char, collisionLayer);

    game.physics.arcade.collide(char, clonesGroup, function(){
        char.alive = false;
    });

    function mahRand(min = 0,max = 1)
    {
        let rand = Math.floor(Math.random()*(max-min+1)+min)
        return (rand == 0 ? 0 : 9 );
    }

    if (char.alive){
        game.physics.arcade.collide(clonesGroup, collisionLayer);
        game.physics.arcade.collide(char, pickupFood, function(maj, ka){
            ka.destroy();
            healthBar.animations.stop("run", true);
            healthBar.animations.play("run");
            healthTimer.destroy();
            healthTimer = game.time.create(false);
            healthTimer.loop(health * 1000, healthTick, this);
            healthTimer.start();

            eat.play()

            exp += 20;
            if (exp == 100){
                level++;
            }

            spawnFood()
        });

        game.physics.arcade.collide(char, pickupBouns, function(maj, ka){
            ka.destroy();
            healthBar.animations.stop("run", true);
            healthBar.animations.play("run");
            healthTimer.destroy();
            healthTimer = game.time.create(false);
            healthTimer.loop(health * 1000, healthTick, this);
            healthTimer.start();

            if(mahRand() > 0){
                if (clones[0]){
                    clones[0].destroy();
                }
            }
            else {
                cloneInterval = 20000;
                cloneTimer = game.time.create(false);
                cloneTimer.loop(cloneInterval, spawnClone, this);
                cloneTimer.start();
            }


            eat.play()

            exp += 20;
            if (exp == 100){
                level++;
            }

            spawnBonus()
        });

        // Reset walking speed to 0
        char.body.velocity.x = 0;
        char.body.velocity.y = 0;
        playerPosition[0] = char.x;
        playerPosition[1] = char.y;

        // Initial speed
        mahSpeed = normalSpeed;

        let animation = "";

        // Spacebar
        if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
            /// if nasledujici policko je hlina
            mahSpeed = 200;
            /// /if
            let pos = moveset[moveset.length - 1].position;
            pos[0] = parseInt(parseInt(pos[0]) / 45);
            pos[1] = parseInt(parseInt(pos[1]) / 45);
            try {
                switch(moveset[moveset.length - 1].direction){
                    case MOVE.UP:
                        if (map.getTile(pos[0],pos[1] -1).index == 9){
                            map.removeTile(pos[0],pos[1] -1 );
                            //map.putTile(0, pos[0],pos[1] -1 );
                        }
                        if (map.getTile(pos[0] -1,pos[1] -1).index == 9){
                            map.removeTile(pos[0] -1,pos[1] -1 );
                            //map.putTile(0, pos[0] -1,pos[1] -1 );
                        }
                        break;
                    case MOVE.DOWN:
                        if (map.getTile(pos[0],pos[1] +1).index == 9){
                            map.removeTile(pos[0],pos[1] +1 );
                            //map.putTile(0, pos[0],pos[1] +1 );
                        }
                        if (map.getTile(pos[0] +1,pos[1] +1).index == 9){
                            map.removeTile(pos[0] +1,pos[1] +1 );
                            //map.putTile(0, pos[0] +1,pos[1] +1 );
                        }
                        break;
                    case MOVE.RIGHT:
                        if (map.getTile(pos[0] + 1,pos[1]).index == 9){
                            map.removeTile(pos[0] +1,pos[1]);
                            //map.putTile(0, pos[0] +1,pos[1]);
                        }
                        if (map.getTile(pos[0] + 1,pos[1] + 1).index == 9){
                            map.removeTile(pos[0] +1,pos[1] +1);
                            //map.putTile(0, pos[0] +1,pos[1] +1);
                        }
                        break;
                    case MOVE.LEFT:
                        if (map.getTile(pos[0] - 1,pos[1]).index == 9){
                            map.removeTile(pos[0] -1,pos[1]);
                            //map.putTile(0, pos[0] -1,pos[1]);
                        }
                        if (map.getTile(pos[0] - 1,pos[1] -1).index == 9){
                            map.removeTile(pos[0] -1,pos[1] -1);
                            //map.putTile(0, pos[0] -1,pos[1] -1);
                        }
                        break;
                }
            }
            catch(e){
                console.log("cosik se posralo");
            }
        }

        // Arrow left / A key
        if (cursors.left.isDown || game.input.keyboard.isDown(Phaser.Keyboard.A))
        {
            char.body.velocity.x = -mahSpeed;
            char.animations.play(animation || "left");

            if (moving){
                if (moveset[moveset.length - 1].direction == MOVE.LEFT){
                    moveset[moveset.length - 1].time = getMoveTime();
                    moveset[moveset.length - 1].position = JSON.parse(JSON.stringify(playerPosition))
                }
                else {
                    resetMoveTimer();

                    moveset.push({
                        direction: MOVE.LEFT,
                        time: getMoveTime(),
                        position: JSON.parse(JSON.stringify(playerPosition))
                    });
                }
            }
            else {
                resetMoveTimer();

                moveset.push({
                    direction: MOVE.LEFT,
                    time: getMoveTime(),
                    position: JSON.parse(JSON.stringify(playerPosition))
                });
                moving = true;
            }
        }
        // Arrow right / D key
        else if (cursors.right.isDown || game.input.keyboard.isDown(Phaser.Keyboard.D))
        {
            char.body.velocity.x = mahSpeed;
            char.animations.play(animation || "right");

            if (moving){
                if (moveset[moveset.length - 1].direction == MOVE.RIGHT){
                    moveset[moveset.length - 1].time = getMoveTime();
                    moveset[moveset.length - 1].position = JSON.parse(JSON.stringify(playerPosition))
                }
                else {
                    resetMoveTimer();

                    moveset.push({
                        direction: MOVE.RIGHT,
                        time: getMoveTime(),
                        position: JSON.parse(JSON.stringify(playerPosition))
                    });
                }
            }
            else {
                resetMoveTimer();

                moveset.push({
                    direction: MOVE.RIGHT,
                    time: getMoveTime(),
                    position: JSON.parse(JSON.stringify(playerPosition))
                });
                moving = true;
            }
        }
        // Arrow down / S key
        else if (cursors.down.isDown || game.input.keyboard.isDown(Phaser.Keyboard.S))
        {
            char.body.velocity.y = mahSpeed;
            char.animations.play(animation || "bottom");

            if (moving){
                if (moveset[moveset.length - 1].direction == MOVE.DOWN){
                    moveset[moveset.length - 1].time = getMoveTime();
                    moveset[moveset.length - 1].position = JSON.parse(JSON.stringify(playerPosition))
                }
                else {
                    resetMoveTimer();

                    moveset.push({
                        direction: MOVE.DOWN,
                        time: getMoveTime(),
                        position: JSON.parse(JSON.stringify(playerPosition))
                    });
                }
            }
            else {
                resetMoveTimer();

                moveset.push({
                    direction: MOVE.DOWN,
                    time: getMoveTime(),
                    position: JSON.parse(JSON.stringify(playerPosition))
                });
                moving = true;
            }
        }
        // Arrow up / W key
        else if (cursors.up.isDown || game.input.keyboard.isDown(Phaser.Keyboard.W))
        {
            char.body.velocity.y = -mahSpeed;
            char.animations.play(animation || "top");

            if (moving){
                if (moveset[moveset.length - 1].direction == MOVE.UP){
                    moveset[moveset.length - 1].time = getMoveTime();
                    moveset[moveset.length - 1].position = JSON.parse(JSON.stringify(playerPosition))
                }
                else {
                    resetMoveTimer();

                    moveset.push({
                        direction: MOVE.UP,
                        time: getMoveTime(),
                        position: JSON.parse(JSON.stringify(playerPosition))
                    });
                }
            }
            else {
                resetMoveTimer();
                moveset[moveset.length - 1].position = JSON.parse(JSON.stringify(playerPosition));
                moveset.push({
                    direction: MOVE.UP,
                    time: getMoveTime(),
                    position: JSON.parse(JSON.stringify(playerPosition))
                });
                moving = true;
            }
        }
        // Idle animation
        else {
            char.animations.play(animation || "idle");

            moving = false;
            if (moveset[moveset.length - 1].direction == MOVE.IDLE){
                moveset[moveset.length - 1].time = getMoveTime();
                moveset[moveset.length - 1].position = JSON.parse(JSON.stringify(playerPosition))
            }
            else {
                resetMoveTimer();

                setTimeout(function(){
                    playerPosition[0] = char.x;
                    playerPosition[1] = char.y;
                    moveset[moveset.length - 1].position = JSON.parse(JSON.stringify(playerPosition));

                    moveset.push({
                        direction: MOVE.IDLE,
                        time: getMoveTime(),
                        position: JSON.parse(JSON.stringify(playerPosition))
                    });
                }, 100);
            }
        }
    }
    else {
        if (!char.animations.getAnimation("death").isPlaying){
            char.body.velocity.x = 0;
            char.body.velocity.y = 0;
            playerPosition[0] = char.x;
            playerPosition[1] = char.y;

            healthBar.animations.getAnimation("run").setFrame(20);
            healthBar.animations.stop("run", false);

            clonesGroup.kill();
            pickupFood.kill();

            char.animations.play("death").onComplete.add(function(){
                cloneTimer.destroy();
                healthTimer.destroy();
                char.kill();

                stateText.text = "Game over";
                stateText.visible = true;
            });
        }
    }
}

function spawnClone(){
    let clone;
    clone = game.add.sprite(playerInitPosition[0], playerInitPosition[1], "clone", 0);
    clone.anchor.setTo(0.5);
    clone.inputEnabled = true;
    clone.animations.add("run", null, fps, true);
    clone.animations.play("run");
    game.physics.enable(clone);

    game.physics.arcade.collide(clone, collisionLayer);

    clones.push(clone);
    clonesGroup.add(clone);
    spawnCloneWalker(clone, 0);
};

function spawnCloneWalker(clone, index){
    clone.body.velocity.y = 0;
    clone.body.velocity.x = 0;

    if (moveset[index]){
        if (moveset[index].direction != MOVE.IDLE){
            game.physics.arcade.moveToXY(clone, moveset[index].position[0], moveset[index].position[1], normalSpeed);
        }
        setTimeout(function(){
            if (moveset.length > index++){
                spawnCloneWalker(clone, index++);
            }
        }, moveset[index].time);
    }
}

function spawnFood(){
    let pickup = game.add.sprite(playerPosition[0] + (Math.floor(Math.random()*(12-9+1)+9) * 45), (Math.random() < 0.5 ? -1 : 1 )*playerPosition[1], "pickup-food", 0);
    pickupFood.add(pickup);
    game.physics.enable(pickup);
    pickup.animations.add("run", null, 10, true);
    pickup.animations.play("run");

    map.removeTile(parseInt(pickup.x / 45), parseInt(pickup.y / 45));
}

function spawnBonus(){
    let bonus = game.add.sprite(playerPosition[0] + (Math.floor(Math.random()*(7-5+1)+5) * 45), playerPosition[1] + (Math.random() < 0.5 ? -1 : 1 )*(Math.floor(Math.random()*(7-5+1)+5)), "bonus", 0);
    pickupBouns.add(bonus);
    game.physics.enable(bonus);
    bonus.animations.add("run", null, 10, true);
    bonus.animations.play("run");

    map.removeTile(parseInt(bonus.x / 45), parseInt(bonus.y / 45));
}

function healthTick(){
    char.alive = false;
}

function dig(sprite, tile){
    console.log("cosik");

    return false;
}

function getMoveTime(){
    return (stageLimit - moveTimer.duration);
}

function resetMoveTimer(){
    moveTimer.destroy();
    moveTimer = game.time.create(true);
    moveTimer.loop(stageLimit, function(){}, this);
    moveTimer.start();
}

function listToMatrix(list, elementsPerSubArray) {
    var matrix = [], i, k;

    for (i = 0, k = -1; i < list.length; i++) {
        if (i % elementsPerSubArray === 0) {
            k++;
            matrix[k] = [];
        }

        matrix[k].push(list[i]);
    }

    return matrix;
}